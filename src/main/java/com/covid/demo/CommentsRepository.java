package com.covid.demo;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CommentsRepository extends CrudRepository<Comments,Long> {
    List<Comments> findAllByPostid(String id);
}
