package com.covid.demo;


import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

@Entity
public class Comments {

    @Id
    @GeneratedValue
    Integer id;

    @Column
    String author;

    @Column
    String value;

    @Column
    String postid;

    public Comments() {
    }

    public Comments(String author, String value, String postid) {
        this.author = author;
        this.value = value;
        this.postid = postid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }
}
