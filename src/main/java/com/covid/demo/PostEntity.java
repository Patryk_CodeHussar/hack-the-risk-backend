package com.covid.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PostEntity {

    @Id
    private String id;

    @Column
    private String title;

    @Column
    private String author;

    @Column
    private String value;

    @Column
    private Integer positive;

    @Column
    private Integer negative;

    public String getId() {
        return id;
    }

    public PostEntity() {
    }

    public PostEntity(String id, String title, String author, String value) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.positive=0;
        this.negative=0;
        this.value=value;
    }

    public void addPositive(){
        positive++;
    }

    public void addNegative(){
        negative++;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getPositive() {
        return positive;
    }

    public void setPositive(Integer positive) {
        this.positive = positive;
    }

    public Integer getNegative() {
        return negative;
    }

    public void setNegative(Integer negative) {
        this.negative = negative;
    }

    public PostEntity(String id, String title, String author, int positive, int negative) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.positive = positive;
        this.negative = negative;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
