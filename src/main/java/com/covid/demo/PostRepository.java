package com.covid.demo;


import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<PostEntity,Long> {

    PostEntity findById(String id);
}