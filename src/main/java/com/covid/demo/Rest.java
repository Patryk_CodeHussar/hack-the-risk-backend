package com.covid.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@CrossOrigin
@RestController
public class Rest {

    Random random = new Random(100);
    @Autowired
    PostRepository postRepository;

    @Autowired
    CommentsRepository commentsRepository;

    public String sha1(String input) {
        String sha1 = null;
        try {
            MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
            msdDigest.update(input.getBytes("UTF-8"), 0, input.length());
            sha1 = DatatypeConverter.printHexBinary(msdDigest.digest());
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
        }
        return sha1;
    }

    @PostMapping("api/addPost")
    public PostEntity addPost(@RequestBody PostEntity postEntity) {
        String id = sha1(postEntity.getTitle() + postEntity.getAuthor() + random.nextInt(100));
        return postRepository.save(new PostEntity(id, postEntity.getTitle(), postEntity.getAuthor(), postEntity.getValue()));
    }

    @GetMapping("/api/addReaction/{id}")
    public void addReaction(@PathVariable String id, @RequestParam(name = "up") String up) {
        PostEntity postEntity = postRepository.findById(id);
        if (up.equals("true")) {
            postEntity.addPositive();
        } else {
            postEntity.addNegative();
        }

        postRepository.save(postEntity);
    }


    // TODO anserw komentarze
    @GetMapping("/api/getPosts")
    public List<PostEntity> getPosts(@RequestParam(name="index") int index, @RequestParam(name="anserw") String anserw){
        List<PostEntity> postEntityList = new ArrayList<>();
        int iterator = 0;
        for (PostEntity postentity:postRepository.findAll()) {
            if((index-1)*10<=iterator || (index-1)*10+9>=iterator){
                if(commentsRepository.findAllByPostid(postentity.getId())!=null)
                   postEntityList.add(postentity);
            }
            iterator++;
        }

        return postEntityList;
    }

    @PostMapping("/api/addAnwser/{id}")
    public void addAnserw(@PathVariable String id, @RequestBody Anserw anserw)
    {
        commentsRepository.save(new Comments(anserw.getAuthor(), anserw.getText(), id));
    }

    @GetMapping("/api/post")
    public PostEntity getPost(@RequestParam(name="id") String id){
        return postRepository.findById(id);
    }

}
